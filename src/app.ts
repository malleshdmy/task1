import bodyParser from 'body-parser';
import cors from 'cors';
import express, {Application} from 'express';
import morgan from 'morgan';

import logger from './utils/logger.util';
import routes from './routes';
import * as path from 'path';

export class App {
    public app: Application;

    constructor(private port?: number | string) {
        this.app = express();
        this.settings();
        this.middleWares();
        this.routes();
    }

    public async listen(): Promise<void> {
        await this.app.listen(this.app.get('port'));
        logger.info('API Server listening ports => ' + this.app.get('port'));
    }

    private settings() {
        this.app.set('port', this.port);
    }

    private middleWares() {
        const corsOptions = {
            origin: '*',
            methods: 'GET, OPTIONS, PUT, POST, DELETE',
        };
        const stream = {
            write: (message: string) => {
                logger.info(message);
            },
        };

        this.app.use(bodyParser.urlencoded({
            limit: '100mb', extended: true,
        }));
        this.app.use(bodyParser.json({
            limit: '100mb',
        }));
        const uploadPath = path.resolve('./public');
        this.app.use(express.static(uploadPath));
        this.app.use(cors(corsOptions));
        this.app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status ' +
            ':res[content-length]', {stream}));
        this.app.get('/', function (req, res) {
            res.send('TASKS-API SERVER HOME');
        });
    }

    private routes() {
        this.app.use(routes);
    }
}

