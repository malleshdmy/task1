import * as path from 'path';
import {config} from 'dotenv';

config({path: path.resolve(__dirname, '../../.env')});

export const PORT = process.env.PORT || 8000;
export const JWT_SECRET = process.env.JWT_SECRETE || 'tasks-api-token';
export const REFRESH_JWT_SECRET = process.env.JWT_SECRETE || 'tasks-api-refresh-token';

export const MONGO_DATABASE = {
    address: process.env.MONGO_DATABASE_ADDRESS || '127.0.0.1',
    port: process.env.DATABASE_PORT || 27017,
    username: process.env.MONGO_DATABASE_USERNAME,
    password: process.env.MONGO_DATABASE_PASSWORD,
    name: process.env.DATABASE_NAME || 'tasks',
};


export function checkEnv() {
    return new Promise(((resolve, reject) => {
        const mandatoryFields = ['MONGO_DATABASE_ADDRESS', 'DATABASE_PORT', 'DATABASE_NAME'];
        mandatoryFields.forEach(field => {
            if (!process.env[field]) {
                reject(`${field} is missing`);
            }
        });
        resolve();
    }));
}
