export const DEFAULT_OTP_LENGTH = 4;
export const DEFAULT_OTP_EXPIRY_TIME = 10 * 60;
export const DEFAULT_OTP = '1234';