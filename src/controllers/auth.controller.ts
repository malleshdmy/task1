import * as authService from '../services/auth.service';
import logger from '../utils/logger.util';
import {errorHandler} from '../utils/error_handler.util';
import {IServiceResponse} from '../models/api';

export async function checkValidMobileNumber(req, res): Promise<void> {
    try {
        const authResponse: IServiceResponse = await authService.checkPhoneExist(req.body);
        res.status(200).send(authResponse);
    } catch (error) {
        logger.error(`ERROR in checkValidMobileNumber() => ${error}`);
        errorHandler(res, error, 'Failed verify mobile number.');
    }
}

export async function login(req, res): Promise<void> {
    try {
        const authResponse: IServiceResponse = await authService.loginUser(req.body);
        res.status(200).send(authResponse);
    } catch (error) {
        logger.error(`ERROR in loginUser() => ${error}`);
        errorHandler(res, error, 'Failed to login.');
    }
}

export async function logout(req, res): Promise<void> {
    try {
        const authResponse: IServiceResponse = await authService.logoutUser(req.body);
        res.status(200).send(authResponse);
    } catch (error) {
        logger.error(`ERROR in logout() => ${error}`);
        errorHandler(res, error, 'Failed to logout.');
    }
}

export async function registerUser(req, res): Promise<void> {
    try {
        const authResponse: IServiceResponse = await authService.addUserRegistration(req.body);
        res.status(200).send(authResponse);
    } catch (error) {
        logger.error(`ERROR in registerUser() => ${error}`);
        errorHandler(res, error, 'Failed to register.');
    }
}

export async function verifyRegisterUserOTP(req, res): Promise<void> {
    try {
        const authResponse: IServiceResponse = await authService.verifyRegistrationOTP(req.body);
        res.status(200).send(authResponse);
    } catch (error) {
        logger.error(`ERROR in verifyRegisterUserOTP() => ${error}`);
        errorHandler(res, error, 'Failed to verify OTP.');
    }
}

// export async function resendRegisterUserOTP(req, res) {
//     try {
//         const data = await authService.resendRegistrationOTP(req.body);
//         res.send({
//             status: 200,
//             data,
//             message: 'OTP sent successfully.',
//             showMessage: true,
//         });
//     } catch (error) {
//         logger.error(`ERROR in resendRegisterUserOTP() => ${error}`);
//         errorHandler(res, error, 'Failed to verify OTP.');
//     }
// }

