import * as chitsService from '../services/chits.service';
import logger from '../utils/logger.util';
import {errorHandler} from '../utils/error_handler.util';
import {IServiceResponse} from '../models/api';

export async function getAllChits(req, res): Promise<void> {
    try {
        const chitResponse: IServiceResponse = await chitsService.fetchAllChits(req.userSession);

        res.status(200).send(chitResponse);
    } catch (error) {
        logger.error(`ERROR in getAllChits() => ${error}`);
        errorHandler(res, error, 'Failed to get Chits.');
    }
}

export async function getChitDetails(req, res): Promise<void> {
    try {
        const chitId: any = req.params.id;
        const chitResponse: IServiceResponse = await chitsService.fetchChitDetails(req.userSession, chitId);

        res.status(200).send(chitResponse);
    } catch (error) {
        logger.error(`ERROR in getChitDetails() => ${error}`);
        errorHandler(res, error, 'Failed to getChitDetails.');
    }
}

export async function saveChit(req, res): Promise<void> {
    try {
        const chitResponse: IServiceResponse = await chitsService.saveChit(req.body);

        res.status(201).send(chitResponse);
    } catch (error) {
        logger.error(`ERROR in saveChit() => ${error}`);
        errorHandler(res, error, 'Failed to saveChit.');
    }
}

