import {MONGO_DATABASE} from './config';
import mongoose from 'mongoose';

export const initializeDBConnection = async () => {
    try {
        const options = {
            poolSize: 16,
            // reconnectTries: 600,
            // reconnectInterval: 500,
            useNewUrlParser: true,
            useCreateIndex: true,
            useFindAndModify: false,
            useUnifiedTopology: true,
        };
        let URL = `mongodb://${MONGO_DATABASE.address}:${MONGO_DATABASE.port}/${MONGO_DATABASE.name}`;
        if (MONGO_DATABASE.username.length && MONGO_DATABASE.password.length) {
            URL = `mongodb://${MONGO_DATABASE.username}:${MONGO_DATABASE.password}@${MONGO_DATABASE.address}:${MONGO_DATABASE.port}/${MONGO_DATABASE.name}`;
        }
        await mongoose.connect(URL, options);
    } catch (error) {
        throw error;
    }
};