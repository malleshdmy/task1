import * as UserDBO from './lib/user';
import * as ChitDBO from './lib/chit';
import * as OtpDBO from './lib/otp';

export {
    UserDBO,
    ChitDBO,
    OtpDBO,
};