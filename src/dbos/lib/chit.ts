import Chit from '../../models/db/chit';

const populatePaths = [{
    path: 'users',
    select: {
        first_name: 1,
        last_name: 1,
        email_address: 1,
        profile_image: 1,
    },
    model: 'user',
}];

export async function save(doc) {
    try {
        const chit: any = new Chit(doc);
        await chit.save();
        return await Chit.populate(chit, populatePaths);
    } catch (error) {
        throw error;
    }
}

export async function findOne(conditions, projections, options) {
    try {
        return await Chit.findOne(conditions, projections, options)
            .populate(populatePaths);

    } catch (error) {
        throw error;
    }
}

export async function find(conditions, projections, options) {
    try {
        return await Chit.find(conditions, projections, options)
            .populate(populatePaths);

    } catch (error) {
        throw error;
    }
}

export async function findOneAndUpdate(conditions, update, options?: any) {
    try {
        return await Chit.findOneAndUpdate(conditions, update, options)
            .populate(populatePaths);

    } catch (error) {
        throw error;
    }
}

export async function findOneAndDelete(conditions, options) {
    try {
        return await Chit.findOneAndDelete(conditions, options)
            .populate(populatePaths);

    } catch (error) {
        throw error;
    }
}
