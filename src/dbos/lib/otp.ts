import Otp from '../../models/db/otp';

const populatePaths = [];

export async function save(doc) {
    try {
        const otp = new Otp(doc);
        await otp.save();
        return await Otp.populate(otp, populatePaths);
    } catch (error) {
        throw error;
    }
}

export async function findOne(conditions, projections, options) {
    try {
        return await Otp.findOne(conditions, projections, options)
            .populate(populatePaths);

    } catch (error) {
        throw error;
    }
}

export async function find(conditions, projections, options) {
    try {
        return await Otp.find(conditions, projections, options)
            .populate(populatePaths);

    } catch (error) {
        throw error;
    }
}

export async function findOneAndUpdate(conditions, update, options) {
    try {
        return await Otp.findOneAndUpdate(conditions, update, options)
            .populate(populatePaths);
    } catch (error) {
        throw error;
    }
}

export async function findOneAndDelete(conditions, options) {
    try {
        return await Otp.findOneAndDelete(conditions, options)
            .populate(populatePaths);

    } catch (error) {
        throw error;
    }
}