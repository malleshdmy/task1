import User from '../../models/db/user';

const populatePaths = [];

export async function save(doc) {
    try {
        const user = new User(doc);
        await user.save();
        return await User.populate(user, populatePaths);
    } catch (error) {
        throw error;
    }
}

export async function findOne(conditions, projections, options) {
    try {
        return await User.findOne(conditions, projections, options)
            .populate(populatePaths);

    } catch (error) {
        throw error;
    }
}

export async function find(conditions, projections, options) {
    try {
        return await User.find(conditions, projections, options)
            .populate(populatePaths);

    } catch (error) {
        throw error;
    }
}

export async function findOneAndUpdate(conditions, update, options) {
    try {
        return await User.findOneAndUpdate(conditions, update, options)
            .populate(populatePaths);
    } catch (error) {
        throw error;
    }
}

export async function findOneAndDelete(conditions, options) {
    try {
        return await User.findOneAndDelete(conditions, options)
            .populate(populatePaths);

    } catch (error) {
        throw error;
    }
}