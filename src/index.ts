import {App} from './app';
import {PORT, checkEnv} from './config';
import {initializeDBConnection} from './database';
import logger from './utils/logger.util';


async function main() {
    try {
        await checkEnv();
        await initializeDBConnection();
        const app = new App(PORT);
        await app.listen();
    } catch (error) {
        logger.error(error);
        process.exit(1);
    }
}

main();
