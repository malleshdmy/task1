import {Request, Response} from 'express';
import logger from '../utils/logger.util';
import {verifyJWT} from '../utils/auth.util';
import {APIError, UserSession} from '../models/api';

export async function isAuthenticated(req: Request, res: Response, next): Promise<void> {
    let token = null;
    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        token = req.headers.authorization.split(' ')[1];
    } else if (req.query && req.query.token) {
        token = req.query.token;
    }
    try {
        if (!token) {
            throw new APIError('Token required.', '402', 'jwtToken');
        }
        const decode: any = await verifyJWT(token);
        // @ts-ignore
        req.userSession = new UserSession(decode.userId, [], []);
        next();
    } catch (error) {
        logger.error(error);
        if (error.message === 'jwt expired') {
            error = new APIError('Token expired.', '400', 'jwtToken');
        }
        next(error);
    }
}
