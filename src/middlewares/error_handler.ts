import {IServiceResponse, ServiceResponse} from '../models/api';

export function errorHandler(error, req, res, next): void {
    const status = +error.status || 500;
    const response: IServiceResponse = new ServiceResponse(error.message, null, true, error,
        [error.message]);
    res.status(status).send(response);
}
