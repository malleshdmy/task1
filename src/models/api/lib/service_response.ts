export interface IServiceResponse {
    message: string;
    data: any;
    showMessage: boolean;
    errors?: any;
    errorMessages?: string[];
}

export class ServiceResponse implements IServiceResponse {
    public message: string;
    public data: any;
    public showMessage: boolean;
    public errors?: any;
    public errorMessages?: string[];

    constructor(message: string, data: any, showMessage: boolean, errors?: any, errorMessages?: string[]) {
        this.message = message;
        this.data = data;
        this.showMessage = showMessage;
        this.errors = errors;
        this.errorMessages = errorMessages;
    }

}