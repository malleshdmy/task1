export interface IUserSession {
    userId: number;
    roles: string[];
    permissions: string[];
}

export class UserSession implements IUserSession {
    public userId: number;
    public roles: string[];
    public permissions: string[];

    constructor(userId: number, roles: string[], permissions: string[]) {
        this.userId = userId || null;
        this.roles = roles || [];
        this.permissions = permissions || [];
    }
}
