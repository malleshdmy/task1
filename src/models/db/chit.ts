import mongoose from 'mongoose';

const chitSchema = new mongoose.Schema({
    amount: {
        type: String,
        required: true,
    },
    start_date: {
        type: Date,
        required: true,
    },
    end_date: {
        type: Date,
        required: true,
    },
    description: {
        type: String,
        required: false,
    },
    users: [{
        type: mongoose.Types.ObjectId,
    }],
    created_at: {
        type: Date,
        required: true,
        default: Date.now,
    },
    modified_at: {
        type: Date,
        required: true,
        default: Date.now,
    },
});

export default mongoose.model('chit', chitSchema);