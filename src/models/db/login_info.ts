import mongoose from 'mongoose';

const loginInfoSchema = new mongoose.Schema({
    access_token: {
        type: String,
        required: true,
    },
    refresh_token: {
        type: String,
        required: true,
    },
    remote_address: {
        type: String,
        required: true,
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now,
    },
    modified_at: {
        type: Date,
        required: true,
        default: Date.now,
    },
});

export default mongoose.model('loginInfo', loginInfoSchema);