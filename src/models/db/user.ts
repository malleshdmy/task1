import mongoose from 'mongoose';

const profileImageSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        default: 'profile_image.png',
    },
    destination: {
        type: String,
        required: true,
        default: 'uploads/default',
    },
    MIME_type: {
        type: String,
        required: true,
        default: 'image/png',
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now,
    },
    modified_at: {
        type: Date,
        required: true,
        default: Date.now,
    },
});

const userSchema = new mongoose.Schema({
    first_name: {
        type: String,
        required: false,
    },
    last_name: {
        type: String,
        required: false,
    },
    email_address: {
        type: String,
        required: true,
    },
    mobile_number: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    profile_image: {
        type: profileImageSchema,
        required: false,
        default: () => ({}),
    },
    verified_user: {
        type: Boolean,
        default: false,
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now,
    },
    modified_at: {
        type: Date,
        required: true,
        default: Date.now,
    },
}, {
    strict: true,
});

export default mongoose.model('user', userSchema);