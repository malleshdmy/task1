import {Router} from 'express';
import * as controller from '../controllers/auth.controller';
import * as validation from '../validations/auth.validation';

const router = Router();
router.route('/mobile-number/verify')
    .post(validation.checkValidMobileNumber, controller.checkValidMobileNumber);
router.route('/login')
    .post(validation.login, controller.login);
router.route('/logout')
    .post(validation.logout, controller.logout);
router.route('/register')
    .post(validation.registerUser, controller.registerUser);
router.route('/register/verify-otp')
    .post(validation.verifyOTP, controller.verifyRegisterUserOTP);
// router.route('/register/resend-otp')
//     .post(validation.resendOTP, controller.resendRegisterUserOTP);


export default router;
