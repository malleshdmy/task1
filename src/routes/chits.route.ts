import {Router} from 'express';
import * as controller from '../controllers/chits.controller';
import * as validation from '../validations/chits.validation';
import {isAuthenticated} from '../middlewares/authentication';
import {errorHandler} from '../middlewares/error_handler';


const router = Router();


router.route('/')
    .get(isAuthenticated, controller.getAllChits, errorHandler)
    .post(isAuthenticated, validation.saveChit, controller.saveChit, errorHandler);
router.route('/:id')
    .put()
    .get(isAuthenticated, controller.getChitDetails, errorHandler)
    .delete();


export default router;
