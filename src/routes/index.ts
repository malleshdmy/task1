import {Router} from 'express';
import authRoutes from './auth.route';
import chitsRoutes from './chits.route';
import swaggerUi from 'swagger-ui-express';
// @ts-ignore
import * as swaggerDocument from '../swagger.json';


const router = Router();

router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
router.use('/', authRoutes);
router.use('/chits', chitsRoutes);


export default router;
