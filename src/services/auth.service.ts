import {comparePasswords, generateRandomOTP, hashPassword} from '../utils/password.util';
import {generateAccessToken, generateRefreshToken, verifyRefreshJWT} from '../utils/auth.util';
import logger from '../utils/logger.util';
import {DEFAULT_OTP, DEFAULT_OTP_EXPIRY_TIME, DEFAULT_OTP_LENGTH} from '../constants/default.constant';
import {OtpDBO, UserDBO} from '../dbos';
import {APIError, IServiceResponse, ServiceResponse} from '../models/api';


export async function checkPhoneExist(payload: any): Promise<IServiceResponse> {
    try {
        const user = await UserDBO.findOne({
            mobile_number: payload.mobileNumber,
        }, {
            first_name: 1,
        }, {});

        return new ServiceResponse('Successfully verified mobile number.', user, false);
    } catch (error) {
        logger.info('Error occurred in checkEmailOrPhoneExist()');
        logger.error(error);
        throw error;
    }
}

export async function loginUser(payload: any): Promise<any> {
    try {
        const {
            mobileNumber,
            password,
        } = payload;
        const user: any = await UserDBO.findOne({
            mobile_number: mobileNumber,
        }, {
            _id: 1,
            email_address: 1,
            first_name: 1,
            last_name: 1,
            password: 1,
            profile_image: 1,
            verified_user: 1,
        }, {});
        if (!user) {
            throw new APIError('User not found', '404', 'mobileNumber');
        }
        const matchedPassword = await comparePasswords(user.password, password);
        if (!matchedPassword) {
            throw new APIError('Invalid password', '400', 'password');
        }
        const accessToken = await generateAccessToken({userId: user._id});
        const refreshToken = await generateRefreshToken({userId: user._id});
        delete user.toObject().password;
        const response = {
            accessToken,
            refreshToken,
            user,
        };
        return new ServiceResponse('Successfully logged into account.', response, false);
    } catch (error) {
        logger.error(`ERROR in login user() => ${error}`, error);
        throw error;
    }
}

export async function logoutUser(payload: any): Promise<IServiceResponse> {
    try {
        return new ServiceResponse('Successfully logged out account.', {}, false);
    } catch (error) {
        logger.error(`ERROR in logoutUser() => ${error}`, error);
        throw error;
    }
}

export async function addUserRegistration(payload: any): Promise<IServiceResponse> {
    try {
        const user: any = await UserDBO.findOne({
            mobile_number: payload.mobileNumber,
        }, {
            _id: 1,
        }, {});
        if (user) {
            throw new APIError('User already exist', '400', 'mobileNumber');
        }
        const dataToInsert = {
            first_name: payload.firstName || null,
            last_name: payload.lastName || null,
            mobile_number: payload.mobileNumber,
            email_address: payload.emailAddress,
            password: await hashPassword(payload.password),
        };
        const userRecord: any = await UserDBO.save(dataToInsert);

        const otpData = {
            user_id: userRecord._id,
            otp: DEFAULT_OTP,
        };
        await OtpDBO.save(otpData);

        const token = await generateRefreshToken({userId: userRecord._id}, DEFAULT_OTP_EXPIRY_TIME);

        return new ServiceResponse('Successfully registered. OTP sent to mobile.', {token}, true);
    } catch (error) {
        logger.error(`ERROR in addUserRegistration() => ${error}`, error);
        throw error;
    }
}

export async function updateUserRegistration(payload): Promise<any> {
    try {
        return UserDBO.findOneAndUpdate({
            _id: payload.userId,
        }, {
            verified_user: true,
        }, {});
    } catch (error) {
        logger.error(`ERROR in updateUserRegistration() => ${error}`, error);
        throw error;
    }
}

export async function verifyRegistrationOTP(payload: any): Promise<IServiceResponse> {
    try {
        const decode: any = await verifyRefreshJWT(payload.token);
        logger.info(`TOKEN DECODE:${JSON.stringify(decode)}`);
        const otpData: any = await OtpDBO.findOne({
            user_id: decode.userId,
            otp: payload.otp,
        }, {otp: 1}, {});
        if (otpData && otpData.otp === payload.otp) {
            await updateUserRegistration(decode);
            await OtpDBO.findOneAndDelete({
                user_id: decode.userId,
                otp: payload.otp,
            }, {});
        } else {
            throw new APIError('Invalid OTP.', '400', 'otp');
        }

        return new ServiceResponse('Successfully verified account.', {}, true);
    } catch (error) {
        logger.error(`ERROR in verifyRegistrationOTP() => ${error}`, error);
        throw error;
    }
}

export async function resendRegistrationOTP(payload) {
    try {
        const decode: any = await verifyRefreshJWT(payload.token);
        logger.info(`TOKEN DECODE:${JSON.stringify(decode)}`);
        await OtpDBO.save({
            user_id: decode.userId,
            otp: DEFAULT_OTP,
        });
        const token = await generateRefreshToken({userId: decode.userId}, DEFAULT_OTP_EXPIRY_TIME);

        return new ServiceResponse('Successfully resent OTP to mobile.', {token}, true);
    } catch (error) {
        logger.error(`ERROR in resendRegistrationOTP() => ${error}`, error);
        throw error;
    }
}


