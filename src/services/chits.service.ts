import logger from '../utils/logger.util';
import {ChitDBO} from '../dbos';
import {IServiceResponse, IUserSession, ServiceResponse} from '../models/api';
import mongoose from 'mongoose';
import {monthDiff} from '../utils/date';

export async function fetchAllChits(loggedInUser: IUserSession): Promise<IServiceResponse> {
    try {
        let chits = await ChitDBO.find({
            users: mongoose.Types.ObjectId(loggedInUser.userId),
        }, {}, {});
        chits = chits.map((chit: any) => {
            chit = chit.toObject();
            chit.monthsCompleted = monthDiff(chit.start_date, new Date());
            return chit;
        });

        return new ServiceResponse('Fetched all chits of user.', chits, false);
    } catch (error) {
        logger.info('Error occurred in fetchAllChits()');
        logger.error(error);
        throw error;
    }
}

export async function fetchChitDetails(loggedInUser: IUserSession, chitId: number): Promise<IServiceResponse> {
    try {
        const chit = ChitDBO.findOne({
            _id: chitId,
            users: loggedInUser.userId,
        }, {$: 1}, {});

        return new ServiceResponse('Fetched chit details.', chit, false);
    } catch (error) {
        logger.info('Error occurred in fetchChitDetails()');
        logger.error(error);
        throw error;
    }
}

export async function saveChit(payload: any): Promise<IServiceResponse> {
    try {
        let chit: any = await ChitDBO.save({
            amount: payload.amount,
            start_date: payload.startDate,
            end_date: payload.endDate,
            description: payload.description || null,
        });
        chit = await ChitDBO.findOneAndUpdate({
            _id: chit._id,
        }, {
            $addToSet: {
                users: payload.users,
            },
        }, {});

        return new ServiceResponse('Created new chit successfully.', chit, true);
    } catch (error) {
        logger.info('Error occurred in saveChit()');
        logger.error(error);
        throw error;
    }
}