import jsonwebtoken from 'jsonwebtoken';
import {JWT_SECRET, REFRESH_JWT_SECRET} from '../config';
import logger from './logger.util';

const generateJWT = (payload: object, secret, expiresIn: number) => {
    return jsonwebtoken.sign(payload, secret, {
        algorithm: 'HS256',
        expiresIn,
    });
};

export async function generateAccessToken(payload: object, expiry = 24 * 60 * 60) {
    try {
        return generateJWT(payload, JWT_SECRET, expiry);
    } catch (error) {
        logger.error(`ERROR in login generateAccessToken() => ${error}`);
    }
}

export async function generateRefreshToken(payload: object, expiry = 30 * 24 * 60 * 60) {
    try {
        return generateJWT(payload, REFRESH_JWT_SECRET, expiry);
    } catch (error) {
        logger.error(`ERROR in login generateRefreshToken() => ${error}`);
    }
}

export const verifyJWT = async (token: string) => {
    return jsonwebtoken.verify(token, JWT_SECRET);
};

export const verifyRefreshJWT = async (token: string) => {
    return jsonwebtoken.verify(token, REFRESH_JWT_SECRET);
};
