const DEFAULT_ERROR_MESSAGE = 'Failed to handle the request.';

export function errorHandler(res: any, error: any, message: string = DEFAULT_ERROR_MESSAGE) {
    const errorResponse = {
        message: message,
        data: null,
        errors: error,
        errorMessage: error.message,
        showMessage: true,
    };
    const status = parseInt(error.errorCode) || error.status || 500;
    res.status(status).send(errorResponse);
}
