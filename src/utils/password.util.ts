import bcrypt from 'bcrypt';

export const generateRandomPassword = (length) => {
    const possible = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';

    let str = '';
    for (let i = 0; i < length; ++i) {
        str += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return str;
};

export const hashPassword = async (plainPassword) => {
    return bcrypt.hash(plainPassword, 10);
};

export const comparePasswords = async (hashedPassword, plainPassword) => {
    return bcrypt.compare(plainPassword, hashedPassword);
};

export const generateRandomOTP = (length) => {
    const possible = '0123456789';

    let str = '';
    for (let i = 0; i < length; ++i) {
        str += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return str;
};
