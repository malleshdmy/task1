import * as Joi from '@hapi/joi';
import {mobileNumberValidation, validate} from './common';

export const checkValidMobileNumber = async (req, res, next) => {
    const schema = Joi.object().keys({
        mobileNumber: Joi.string().required(),
    });
    await validate(schema, req, res, next);
};

export const login = async (req, res, next) => {
    const schema = Joi.object().keys({
        mobileNumber: Joi.string().required(),
        password: Joi.string().required(),
    });
    await validate(schema, req, res, next);
};

export const logout = async (req, res, next) => {
    const schema = Joi.object().keys({
        refreshToken: Joi.string().required(),
    });
    await validate(schema, req, res, next);
};

export const registerUser = async (req, res, next) => {
    const schema = Joi.object().keys({
        firstName: Joi.string(),
        lastName: Joi.string(),
        mobileNumber: mobileNumberValidation,
        emailAddress: Joi.string().email().required(),
        password: Joi.string().required(),
    });
    await validate(schema, req, res, next);
};


export async function resendOTP(req, res, next) {
    const schema = Joi.object().keys({
        token: Joi.string().required(),
    });
    await validate(schema, req, res, next);
}

export async function verifyOTP(req, res, next) {
    const schema = Joi.object().keys({
        token: Joi.string().required(),
        otp: Joi.string().required(),
    });
    await validate(schema, req, res, next);
}
