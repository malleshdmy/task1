import * as Joi from '@hapi/joi';
import {isoDate, mobileNumberValidation, validate} from './common';

export const saveChit = async (req, res, next) => {
    const schema = Joi.object().keys({
        amount: Joi.number().required(),
        startDate: isoDate().required(),
        endDate: isoDate().required(),
        description: Joi.string(),
        users: Joi.array().items(Joi.string()),
    });
    await validate(schema, req, res, next);
};


