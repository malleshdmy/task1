import * as JoiBase from '@hapi/joi';
import JoiDate from '@hapi/joi-date';
import logger from '../utils/logger.util';
import {APIError, IAPIError, IServiceResponse, ServiceResponse} from '../models/api';

const Joi = JoiBase.extend(JoiDate);

export function isoDate() {
    return Joi.date().format('YYYY-MM-DD');
}

export function mobileNumberValidation() {
    return Joi.string().min(10).pattern(/^\+?([0-9]{1,3})?-?\s?[1-9]{1}[0-9]{9}$/);
}

export function pincodeValidation() {
    return Joi.string().pattern(/^[0-9]{6}$/);
}

export function addressSchema() {
    return Joi.object().keys({
        addressLine1: Joi.string().required()
            .messages({
                'any.required': 'Address line1 is required.',
            }),
        addressLine2: Joi.string().allow('', null),
        zipcode: pincodeValidation().required()
            .messages({
                'any.required': 'Zipcode is required.',
                'string.pattern.base': 'Pincode should be length of 6 digits.',
            }),
        state: Joi.string().required()
            .messages({
                'any.required': 'State required.',
            }),
        district: Joi.string().required()
            .messages({
                'any.required': 'District is required.',
            }),
        city: Joi.string().required()
            .messages({
                'any.required': 'City is required.',
            }),
    });
}

const buildUsefulErrorObject = (errors: any): IServiceResponse => {
    const usefulErrors: IAPIError[] = [];
    const errorMsgs: string[] = errors.error.message.split('. ');
    for (const error of errors.error.details) {
        if (!usefulErrors.hasOwnProperty(error.path.join('_'))) {
            usefulErrors.push(new APIError(error.message, error.type, error.path.join('_')));
        } else {
            logger.debug('missed error:' + error);
        }
    }
    return new ServiceResponse('Please fill all Mandatory fields with valid values!.', null,
        true, usefulErrors, errorMsgs);
};

export const validate = async (schema, req, res, next) => {
    logger.info('START of common.validator.validate()');
    schema = schema.append({
        token: Joi.string().allow(''),
    });
    let body = Object.assign({}, req.params, req.query);
    if (req.method === 'POST' || req.method === 'PUT') {
        body = Object.assign(body, req.body);
    }
    const result = await schema.validate(body, {abortEarly: false});
    if (result.error) {
        logger.debug(JSON.stringify(result));
        const errorResponse = buildUsefulErrorObject(result);
        res.status(422).send(errorResponse);
    } else {
        next();
    }
};
